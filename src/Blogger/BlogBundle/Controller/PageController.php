<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{

    public function indexAction()
    {
        return $this->render('BloggerBlogBundle:Page:index.html.twig');
    }


    public function aboutAction(){
        return $this->render('BloggerBlogBundle:Page:about.html.twig');
    }


    public function contactAction(Request $request){
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // Выполнение некоторого действия, например, отправка письма.

            // Редирект - это важно для предотвращения повторного ввода данных в форму,
            // если пользователь обновил страницу.
            return $this->redirect($this->generateUrl('BloggerBlogBundle_PageContact'));
        }

        return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }


    public function helloAction($name)
    {
        return $this->render('BloggerBlogBundle:Page:hello.html.twig', array('name' => $name));
    }
}
